import './App.css';
import Section from '../src/Cabecera/seccion-uno';
import Tarjet from '../src/PrimeraSeccion/tarjeta-uno';
import Tarjet1 from './SegundaSeccion/tarjeta-dos';

function App() {
  return (
    <div>
      <Section />
      <Tarjet />
      <Tarjet1 />
    </div>
  );
}

export default App;
