import React from 'react';
import "./tarjeta-dos.css";
import Per1 from './follow-im1';
import Per2 from './follow-im2';
import Per3 from './follow-im3';
import Per4 from './follow-im4';

const Tarjet1=(props)=>{
    return(
        <div className="general">
            <div className="encabezado">
                <div className="seccion-inicio">
                    <p className="titulo">Overview - Today</p>
                    <div className="follow-conten">
                        <p>Page Views
                            <br/> 
                            <br/>87
                        </p>
                        <Per1 />
                        <p className="activ1"><span>3%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Likes
                            <br/> 
                            <br/>52
                        </p>
                        <Per1 />
                        <p className="activ1"><span>2%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Likes
                            <br/> 
                            <br/>5462
                        </p>
                        <Per3 />
                        <p className="activ1"><span>2257%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Profile Views
                            <br/> 
                            <br/>52K
                        </p>
                        <Per3 />
                        <p className="activ1"><span>1375%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Retweets
                            <br/> 
                            <br/>117
                        </p>
                        <Per2 />
                        <p className="activ1"><span>303%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Likes
                            <br/> 
                            <br/>507
                        </p>
                        <Per2 />
                        <p className="activ1"><span>553%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Likes
                            <br/> 
                            <br/>107
                        </p>
                        <Per4 />
                        <p className="activ1"><span>19%</span></p>
                    </div>
                    <div className="follow-conten">
                        <p>Total Views
                            <br/> 
                            <br/>1407
                        </p>
                        <Per4 />
                        <p className="activ1"><span>12%</span></p>
                    </div>

                </div>
            </div>
        </div>
    );
}
export default Tarjet1;