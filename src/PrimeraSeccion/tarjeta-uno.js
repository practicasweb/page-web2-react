import React from 'react';
import "./tarjeta-uno.css";
import Perf1 from './follow-img1';
import Perf2 from './follow-img2';
import Perf3 from './follow-img3';
import Perf4 from './follow-img4';

const Tarjet=(props)=>{
    return(
        <div className="general">
            <div className="encabezado">
                <div className="contenido">
                    <div className="contenedor-seguidor">
                        <p className="usuario">
                            <Perf1 />
                            @nathanf
                        </p>
                        <p className="texto">1987</p>
                        <p className="text">FOLLOWERS</p>
                        <p className="activo1"><span>12 Today</span></p>
                        <div className="borde1"></div>
                    </div>
                    <div className="contenedor-seguidor">
                        <p className="usuario">
                            <Perf2 />
                            @nathanf
                        </p>
                        <p className="texto">1044</p>
                        <p className="text">FOLLOWERS</p>
                        <p className="activo1"><span>99 Today</span></p>
                        <div className="borde2"></div>
                    </div>
                    <div className="contenedor-seguidor">
                        <p className="usuario">
                            <Perf3 />
                            @realnathanf
                        </p>
                        <p className="texto">11K</p>
                        <p className="text">FOLLOWERS</p>
                        <p className="activo1"><span>1099 Today</span></p>
                        <div className="borde3"></div>
                    </div>
                    <div className="contenedor-seguidor">
                        <p className="usuario">
                            <Perf4 />
                            Nathan F.
                        </p>
                        <p className="texto">8239</p>
                        <p className="text">SUBSCRIBERS</p>
                        <p className="activo"><span>144 Today</span></p>
                        <div className="borde4"></div>
                    </div>
                </div>
            </div>
        </div>
    );

}
export default Tarjet;